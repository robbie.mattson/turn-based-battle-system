# Turn-based battle system


I wanted to make a turn-based battle system in Unity that I could use in multiple projects. I created
a battle system with interchangeable battle scenes, where enemies and their placement can be different
between scenes. You can have up to four party members, each with their own abilities and stats. Upon reflection 
I learned quite a bit more about how classes interact with Unity, and mathematics of Party Member stats
in a turn-based battle system. 